# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.http import content_disposition, dispatch_rpc, request, \
    serialize_exception as _serialize_exception, Response
from odoo.addons.website.controllers.main import Home
from datetime import date, timedelta, datetime
from passlib.context import CryptContext
from odoo.exceptions import AccessError, UserError, ValidationError, Warning
from datetime import date
import json
import email.message
import re
import smtplib
import ssl
import werkzeug.utils
import random
import string
import requests
import logging
_logger = logging.getLogger()



   
class Covid19(http.Controller):
     @http.route('/covid19', auth='public', website=True)
     def index(self, **kw):
        covid = request.env['covid.covid_19'].search([])
        return http.request.render('covid_19.listing', {
            "x": covid
         })



     @http.route('/covid19/registrar', type="http", auth='public', website=True)
     def list(self, **kw):
         registro = request.env['covid.covid_19'].search([])
         return http.request.render('covid_19.registrar', {
             "reg":registro
         })
      
     @http.route('/register_covid19', type="http", methods=['POST'], auth='public', website=True)
     def register_covid19(self, **post):
        object_covid_19 = http.request.env["covid.covid_19"]
        # O request.env["covid19"] tambien es valido

        data = {
            "source": post["source"],
            "date": post["date"],
            "country_id": post["country_id"],
            "infected": post["infected"],
            "recovered": post["recovered"],
            "deseaced": post["deseaced"]
        }

        result = object_covid_19.sudo().create()

        if result:
            return json.dumps({"status": 200,"response": "Registrado exitosamente"})
        else:
            return json.dumps({"status": 500,"response": "Error al registrar"})