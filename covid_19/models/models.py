# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class covid_19(models.Model):
     _name = 'covid.covid_19'
     _order = 'id desc'

     source = fields.Char(string='Fuente',required=True)
     date = fields.Datetime(string='Fecha',required=True,default=fields.Datetime.now( ))
     country_id = fields.Many2one('res.country',string='Pais',required=True)
     infected = fields.Integer(string='Infectados',required=True,default=0)
     recovered = fields.Integer(string='Recuperados',required=True,default=0)
     deseaced = fields.Integer(string='Fallecidos',required=True,default=0)
     total_infected = fields.Integer(string='Total Infectados',compute='set_total_infected',required=True,default=0)
     total_recovered = fields.Integer(string='Total Recuperados',compute='set_total_recovered',required=True,default=0)
     total_deseaced = fields.Integer(string='Total Fallecidos',compute='set_total_deseaced',required=True,default=0)
     
     @api.constrains('infected','recovered','deseaced','set_total_infected','set_total_recovered','set_total_deseaced')
     def _check_error(self):
          if self.recovered <0 or self.infected < 0 or self.deseaced < 0:
               raise ValueError("No puedes introducir numeros negativos" )
          elif self.recovered > self.infected or self.deseaced > self.infected:
               raise ValueError("Tanto el Numero de fallecidos como el numero de recuperados no pueden ser mayor que el Numero de infectados \n Debes introducir los valores cuyo número de infectados sea superior " )
          elif self.infected - self.deseaced < self.recovered:
               raise ValueError("El Numero de recuperados no pueden ser mayor que el Numero de infectados y fallecidos, Es decir, estas introduciendo un total de fallecidos que va descontando el numero de infectados, lo cual el numero de recuperados debe estar acorde al numero de infectados restantes")                
     def set_total_infected(self):
           
          for data in self:
               domain=[
                         ('country_id','=',data.country_id.id),
                         ('date','<',data.date),
               ]     
               records=self.search(domain)
               Infecteds=records.mapped('infected')
               data.total_infected=sum(Infecteds)+data.infected
          
     def set_total_recovered(self):
          for data in self:
               domain=[
                         ('country_id','=',data.country_id.id),
                         ('date','<',data.date),
               ]     
               records=self.search(domain)
               Recovereds=records.mapped('recovered')
               data.total_recovered=sum(Recovereds)+data.recovered
     
     def set_total_deseaced(self):
          for data in self:
               domain=[
                         ('country_id','=',data.country_id.id),
                         ('date','<',data.date),
               ]     
               records=self.search(domain)
               Deseaceds=records.mapped('deseaced')
               data.total_deseaced=sum(Deseaceds)+data.deseaced
     def set_percentage_infected(self):
        total=0
        if self.infected:
            total=(self.infected*100)/self.total_infected
        return total
        
     def set_percentage_recovered(self):
        total=0
        if self.recovered:
            total=(self.recovered*100)/self.total_recovered
        return total 
        
     def set_percentage_deseaced(self):
        total=0
        if self.deseaced:
            total=(self.deseaced*100)/self.total_deseaced
        return total
     

     #     value2 = fields.Float(compute="_value_pc", store=True)
#   description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
