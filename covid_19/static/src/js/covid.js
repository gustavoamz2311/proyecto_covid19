odoo.define("covid_19.covid_19", function (require) {
    "use strict";
    
    //En la primera linea carpeta_modulo.Identificador hace referencia a un identificador de nuestro archivo
  
    var ajax = require("web.ajax");
    var utils = require("web.utils");
    var sAnimations = require("website.content.snippets.animation");
  
    //Primeramente los requires, que se refieren a funcionalidades del modulo web de Odoo
  
    sAnimations.registry.identificadorAnimations = sAnimations.Class.extend({  
      selector: "#tablas",
      //El atributo selector de nuestro sAnimations se refiere a sobre que elemento vamos a estar leyendo eventos de
      // sus elementos hijos
      read_events: {
        "click #registrar": "registrarCovid19",
      },
      //Con read_events definimos tres cosas, el evento que vamos a leer (click,change,focus,keyup...etc)
      start: function () {
        var self = this
        return this._super.apply(this, arguments);
        //Esta funcion start se refiere a que queremos nosotros hacer al momento en el que cargue la página donde se encuentra
        //nuestro elemento seleccionado en selector
      },
      registrarCovid19:function(event){ 
        let source = $("#source").val()
        let date = $("#date").val()
        let country_id = $("#country_id").val()
        let infected = $("#infected").val()
        let recovered = $("#recovered").val()
        let deseaced = $("#deseaced").val()
  
        let post = {
          source: source,
          date: date,
          country_id: country_id,
          infected: infected,
          recovered: recovered,
          deseaced: deseaced
        }
  
        //armamos nuestro diccionario post, que contendra en claves los valores correspondientes de lo que vamos a registrar
  
        const url = "/register_covid19"
  
        ajax.post(url, post).then((response) => {
          var res = JSON.parse(response);
          
          if(res["status"] == 200){
              alert(res["response"])
          }else{
              alert(res["response"])
          }
        });
      },
    });
  });
  