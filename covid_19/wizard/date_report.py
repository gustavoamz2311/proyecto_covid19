# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError

class DateReportWizard(models.TransientModel):
    _name = "covid_19.date.report.wizard"
    _description = "Reporte por rango de fechas y por Paises"
    
    
    start_date=fields.Date('Fecha de inicio',required=True)
    end_date=fields.Date('Fecha Final',required=True)
    country_ids = fields.Many2many(
                                    "res.country", 
                                    string="Paises", 
                                    help="Paises que desea generar el reporte:"
                                    )

    def print_report(self):
        Covid19=self.env['covid.covid_19']
        domain=[
                ('date','>',self.start_date),
                ('date','<',self.end_date)
                ]
        if self.country_ids:
            domain.append(('country_id','in',self.country_ids.ids))
        covidField=[
                'source',
                'date',
                'country_id',
                'infected',
                'recovered',
                'deseaced',
                ]
        CovidRecords=Covid19.search_read(domain,covidField)
        data={
            #~ 'doc_ids': self.ids,
            #~ 'doc_model': self.env['covid_19.date.report.wizard'],
            #~ 'docs': self,
            'CovidRecords':CovidRecords,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'country_ids': self.country_ids,
                }
        if self.start_date > self.end_date:
             raise ValidationError("La fecha de Inicio debe ser inferior a la fecha final" )
        else:
                    
            return self.env.ref('covid_19.report_DateReportExternalayout').report_action(self, data=data)
